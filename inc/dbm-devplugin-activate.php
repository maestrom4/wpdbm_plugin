<?php
/**
 * Activate Plugin DBMdevplugin
 *
 * @package dbm-devplugin
 * @version 1.0
 * @author Danilo B. Matias Jr.
 * @author URI: danilomatias.bid
 * @license: GPLV2 or later
 * Text Domain: Danmats Plugin
 */
class DbmDevPluginActivate {
	public static function activate( )
	{	
		flush_rewrite_rules();
	}
}