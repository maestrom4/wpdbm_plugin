<?php
/**
 * Deactivate Plugin DBMdevplugin
 *
 * @package dbm-devplugin
 * @version 1.0
 * @author Danilo B. Matias Jr.
 * @author URI: danilomatias.bid
 * @license: GPLV2 or later
 * Text Domain: Danmats Plugin
 */
class DbmDevPluginDeActivate {
	public static function deactivate ( )
	{
		flush_rewrite_rules();
	}
}