<?php
/**
 * Plugin Name: Dbm Dev Plugin
 * Plugin URI: http://danilomatias.bid
 * Description: A custom plugin for Wordpress example.
 * Version: 1.0
 * Author: Danilo B. Matias Jr.
 * Author URI: danilomatias.bid
 * License: GPLV2 or later
 * Text Domain: Danmats Plugin
 */

defined( 'ABSPATH' ) || die( 'Forbidden access!' );
if ( file_exists( __FILE__ ) . '/vendor/autoload.php' ){
	require_once dirname( __FILE__ ) . '/vendor/autoload.php';
}
class DbmDevPlugin
{
	public $plugin_name;

	public function __construct(){
		$this->plugin_name = plugin_basename(__FILE__);
	}
	/**
	 * Intialization of the plugin.
	 */

	function register() {
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue' ) );
		add_action( 'admin_menu', array( $this, 'add_admin_pages' ) );
		add_filter( "plugin_action_links_$this->plugin_name", array( $this, 'settings_link') );

	}
	public function settings_link($links){
		// add custom settings link
		$settings_link = '<a href="admin.php?page=dbm-plugin">DBM settings</a>';
		array_push( $links, $settings_link );
		return $links;
	}
	public function add_admin_pages() {
		add_menu_page( 'DBM Plugin', 'DBM Plugin', 'manage_options', 'dbm-plugin', array( $this, 'admin_index' ), 'dashicons-store', 110 );
	}
	public function admin_index() {
		require_once plugin_dir_path( __FILE__ ) . 'templates/admin.php';
	}

	//Activation
	function activate() {
		require_once plugin_dir_path( __FILE__ ) . 'inc/dbm-devplugin-activate.php';
		DbmDevPluginActivate::activate();

	}
	function enqueue() {
		wp_enqueue_style( 'dbmpluginstyle', plugins_url( '/assets/dbmstyle.css', __FILE__ ) );
		wp_enqueue_style( 'dbmplugscript', plugins_url( '/assets/dbmscript.js', __FILE__ ) );
	}
	
}

if ( class_exists( 'DbmDevPlugin' ) ) {
	$dbmdevplugin = new DbmDevPlugin();
	$dbmdevplugin->register();
}

// activation
register_deactivation_hook( __FILE__, array( $dbmdevplugin, 'activate' ) );


//deactivate
require_once plugin_dir_path( __FILE__ ) . 'inc/dbm-devplugin-deactivate.php';
register_deactivation_hook( __FILE__, array( 'DbmDevPluginDeActivate', 'deactivate' ) );



